FROM marcel/nuclide-remote
MAINTAINER Kyunghoob Kang <kyunghoon@gmail.com>

RUN npm uninstall -g nuclide && \
    npm install -g nuclide@0.184.0

# Start ssh service
CMD ["/usr/sbin/sshd", "-D"]

